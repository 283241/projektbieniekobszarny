from myhdl import block, always_seq, Signal, intbv, enum, instances


@block
def my_sum(clk, reset, axis_s_raw, axis_m_sum):
    state_t = enum('READ', 'WRITE')
    accumulator = Signal(intbv(0)[32:])
    counter = Signal(intbv(0)[32:])
    state = Signal(state_t.READ)

    IN_values = [Signal(intbv(0)[16:]) for _ in range(4)]

    coef_0 = Signal(intbv(4)[16:])
    coef_1 = Signal(intbv(10)[16:])
    coef_2 = Signal(intbv(10)[16:])
    coef_3 = Signal(intbv(6)[16:])

    @always_seq(clk.posedge, reset=reset)
    def sum_proc():
        if state == state_t.READ:
            axis_m_sum.tvalid.next = 0
            axis_m_sum.tlast.next = 0
            axis_s_raw.tready.next = 1
            if axis_s_raw.tvalid == 1:

                print("DATA INPUT:")
                print(axis_s_raw.tdata.next)
                IN_values[0].next = axis_s_raw.tdata.next
                IN_values[1].next = IN_values[0]
                IN_values[2].next = IN_values[1]
                IN_values[3].next = IN_values[2]

                coef_0.next = coef_0.val
                coef_1.next = coef_1.val
                coef_2.next = coef_2.val
                coef_3.next = coef_3.val

                accumulator.next = (IN_values[0].next * coef_0.next) + (IN_values[1].next * coef_1.next) + (IN_values[2].next * coef_2.next) + (
                            IN_values[3].next * coef_3.next)

                counter.next = counter + 1
                state.next = state_t.WRITE

        elif state == state_t.WRITE:
            axis_s_raw.tready.next = 0
            axis_m_sum.tvalid.next = 1

            if axis_s_raw.tlast.val == 1:
                axis_m_sum.tlast.next = 1
            else:
                axis_m_sum.tlast.next = 0

            axis_m_sum.tdata.next = accumulator
            print("DATA_OUT:")
            print(axis_m_sum.tdata.next)
            if axis_m_sum.tready == 1:
                state.next = state_t.READ
                accumulator.next = 0
        else:
            raise ValueError("Undefined state")

    return instances()
